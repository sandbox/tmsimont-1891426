<?php

/**
 * @file
 * Default rule configurations for Commerce Split Checkout.
 */
function commerce_split_checkout_default_rules_configuration() {
  $rules_restore_saved_cart = '{ "rules_restore_saved_cart" : {
    "LABEL" : "Restore saved cart",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "10",
    "REQUIRES" : [ "commerce_split_checkout", "commerce_checkout" ],
    "ON" : [ "commerce_checkout_complete" ],
    "DO" : [
      { "commerce_split_checkout_restore_saved_carts" : { "commerce_order" : [ "commerce_order" ] } }
    ]
  }
}';
  $configs['rules_restore_saved_cart'] = rules_import($rules_restore_saved_cart);

  return $configs;
}
