<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */
 
 
/**
 * A module uses this hook to define "checkout modes" by returning an array
 *
 * Each checkout mode has landing page that will present the "split checkout" form
 * and options for the end user to proceed.
 *
 * By default, this module will handle page callbacks and access control parameters
 * for menu items defined here.  Modules implementing this hook
 * will be able to override these defaults.
 *
 * The array returned should look like this:
 * 
 * array(
 *   "[checkout mode machine name]" => array(
 *    "path" => "[checkout mode landing page path]",
 *    "menu" => array( [hook_menu parameters] )
 *    "split_page_callback" => "[see example_module_split()]",
 *    "split_page_new_order_goto" => "[see example_module_split_new_goto()]",
 *    "split_page_add_to_cart_goto" => "[see example_module_split_add_goto()]",
 *   ) 
 * )
 *
 * @see example_module_split
 * @see example_module_split_new_goto
 * @see example_module_split_add_goto
 *
 *
 * @see commerce_split_checkout_menu() in commerce_split_checkout.module
 */
function hook_commerce_split_checkout_mode_info() {
  return array(
    'cmode_a' => array(
      'path' => 'example',
      'menu' => array(
        'title' => 'Page title',
      ),
      'split_page_callback' => 'example_module_split',
      'split_page_new_order_goto' => 'example_module_split_new_goto',
      'split_page_add_to_cart_goto' => 'example_module_split_add_goto'
    )
  );
}

/**
 * Callback designated in hook_commerce_split_checkout_mode_info()
 * This is what is returned as a page to the user above the split checkout form
 *
 * @see example_module_form_alter()
 *
 * @param $order
 * @param $cmode
 */
function example_module_split(&$order, $cmode) {
  // manipulate $order based on $cmode
  
  //determine size of order
  $quantity = 0;
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    $quantity += $line_item_wrapper->quantity->value();
  }
  $isare = $quantity>1?"are":"is";
  $itemitems = $quantity>1?"items":"item";
  
  //build message to user
  $message = "
    <p>There $isare currently $quantity $itemitems in your shopping cart.  You have 2 options:</p>
    <ol>
      <li>Go to a page where an item can be added to your current cart.</li>
      <li>Save your shopping cart $itemitems for later and start a new cart.</li>
    </ol>
    <p>How would you like to proceed?</p>
   ";
   
  // use hook_form_alter() to modify the actual form
  
  return $message;
}



/**
 * Callback to send the user somewhere after order is split or doesn't
 * need splitting.
 * 
 * @param $order
 * @param $user
 * @param $cmode
 *
 * @return an array that can be used by drupal_goto()
 */
function example_module_split_new_goto($order, $user, $cmode) {
  return array('checkout/' . $order->order_id, array('query' => array('cmode'=>$cmode)));
}


/**
 * Callback to send the user when they choose to add item to cart
 * 
 * @param $order
 * @param $user
 * @param $cmode
 *
 * @return an array that can be used by drupal_goto()
 */
function example_module_split_add_goto($order, $user, $cmode) {
  return array('node/165', array());
}


/**
 * Implements hook_form_alter().
 * This is an example of how the checkout split form can be modified
 */
function example_module_form_alter(&$form, &$form_state, $form_id) { 
  /**
   * Commerce split checkout
   */
  if ($form_id=="commerce_split_checkout_split_form") {
    if ($form['cmode']['#value'] == 'cmode_a') {
      //alter button text
      $form['buttons']['add_to_cart']['#value'] = "Add this to existing cart";
      //make any other changes to the form
    }
  }
}