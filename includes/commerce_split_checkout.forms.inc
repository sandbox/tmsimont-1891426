<?php



/**
 * builds a form that provides 2 options: 
 * 1) add an item to the existing cart
 * 2) create a brand new order
 */
function commerce_split_checkout_split_form($form, &$form_state, $cmode, $module) {
  $form = !empty($form)?$form:array();
  $form['buttons'] = array(
    'add_to_cart' => array(
      '#type' => 'button',
      '#value' => "Add to cart",
      '#submit' => array('_commerce_split_checkout_split_add_to_cart'),
      '#executes_submit_callback' => true
     ),
     'new_order' => array(
      '#type' => 'button',
      '#value' => "Save cart for later",
      '#submit' => array('_commerce_split_checkout_split_new_order'),
      '#executes_submit_callback' => true
     )
  );
  $form['cmode'] = array(
    '#type' => 'hidden',
    '#value' => $cmode
  );
  $form['module'] = array(
    '#type' => 'hidden',
    '#value' => $module
  );
  return $form;
}



/**
 * callback for "create a brand new order" option selection.
 * wipe out the current order by creating a new one for this user
 * @see commerce_split_checkout_router() for similar use of $info[$cmode]['split_page_new_order_goto']
 */
function _commerce_split_checkout_split_new_order(&$form, &$form_state) {
  global $user;
  $cmode = $form_state['values']['cmode'];
  $module = $form_state['values']['module'];
  
  //save old order
  $order = commerce_cart_order_load($user->uid);
  commerce_order_status_update($order, 'saved_cart');
  
  //wipe out any previously "saved" orders to prevent infinite "saved order" additions
  $all_cart_orders = commerce_split_checkout_commerce_get_all_cart_orders($user->uid);  
  foreach($all_cart_orders as $existing) {
    if ($existing->order_number != $order->order_number) {
      commerce_order_delete($existing->order_id);
    }
  }
  
  //generate a new blank order
  $new_order = _commerce_split_checkout_get_new_order(false, $user, $cmode);
  
  //get information from the module about a funciton that affect behavior here
  $info = module_invoke($module, "commerce_split_checkout_mode_info");
  $path_get = $info[$cmode]['split_page_new_order_goto'];
  if ($new_path = $path_get($new_order,$user,$cmode)) {
    $path = $new_path;
  }
  
  $form_state['redirect'] = $path;
}