<?php


/**
 * page callback for a split checkout process
 */
function commerce_split_checkout_router($module, $cmode) {
  global $user;
  
  /** 
   * the below behavior will only show the "split" page if there is 
   * an order in the current user's cart.
   * if there is not, a dummy order is created and the user is redirected
   * to checkout.
   * That would cause problems without the "Empty checkout" module, and would
   * do no good at all unless there are required line-items on the checkout page,
   * like the donation line item or GCA Membership line item.
   * @note: the "unsplit" functionality does nothing here, and is passed to the module
   */
  //check to see if this user has started an order
  $order = commerce_cart_order_load($user->uid);
  
  if (_commerce_split_checkout_user_order_to_split($order)) {
    //if there's an order with items in it, then the split page is displayed
    return _commerce_split_checkout_split_page($module, $cmode, $order);
  } else {
    //generate a new blank order if necessary
    $order = _commerce_split_checkout_get_new_order($order, $user, $cmode);
  
    //get information from the module about where to go next
    $info = module_invoke($module, "commerce_split_checkout_mode_info");
    $path_get = $info[$cmode]['split_page_new_order_goto'];
    if ($new_path = $path_get($order,$user,$cmode)) {
      $path = $new_path;
    }
  }
  
  if (!isset($path)) {
    if ($order) {
      //by default we will go to checkout if order should not be split
      $path = array('checkout/' . $order->order_id, array('query' => array('cmode'=>$cmode)));
    } else {
      //this shouldn't be possible unless _commerce_split_checkout_user_order_to_split() fails
    }
  }
  
  //when not going to the split page, go to some other destination
  drupal_goto($path[0], $path[1]);
}



/**
 * page callback for pre-checkout processes.  this callback allows
 * modules via hook_commerce_split_checkout_mode_info() to provide a 
 * page callback that will affect the behavior of this page
 */
function _commerce_split_checkout_split_page($module, $cmode, $order) {
  $output = "";
  
  //get information from the module about a funciton that can alter this page
  $info = module_invoke($module, "commerce_split_checkout_mode_info");
  
  //note requirement of $info[cmode]['split_page_callback']
  //TODO: handle this as missing?
  $func = $info[$cmode]['split_page_callback'];
  
  //get a form to display options to the user
  $form = drupal_get_form("commerce_split_checkout_split_form", $cmode, $module);
  
  //pass the form, order and cmode to the module callback
  //TODO: make this make more sense?
  $output .= $func($order, $cmode);
  
  $output .= render($form);
    
  return $output;
}