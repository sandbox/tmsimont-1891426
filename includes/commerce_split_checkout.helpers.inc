<?php



/**
 * Determine if an order has been started for the user, and if that order has
 * any items
 */
function _commerce_split_checkout_user_order_to_split($order) {
  $quantity = 0;
  if($order) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      $quantity += $line_item_wrapper->quantity->value();
    }
  } else {
    return false;
  }
  if ($quantity==0) {
    return false;
  }
  return true;
}


/**
 * Callback for "add to cart" option.  This simply takes the user to a 
 * product display page where the user adds an item to the cart.
 */
function _commerce_split_checkout_split_add_to_cart(&$form, &$form_state) {
  global $user;
  $cmode = $form_state['values']['cmode'];
  $module = $form_state['values']['module'];
  
  $order = commerce_cart_order_load($user->uid);
  
  //get information from the module about a funciton that affect behavior here
  $info = module_invoke($module, "commerce_split_checkout_mode_info");
  $path_get = $info[$cmode]['split_page_add_to_cart_goto'];
  if ($new_path = $path_get($order, $user, $cmode)) {
    $form_state['redirect'] = $new_path;
  }
}

/**
 * This returns a new order for a given user
 */
function _commerce_split_checkout_get_new_order($order, $user, $cmode) {
  //build a new empty order so checkout doesn't bounce on no order.
  if (empty($order) || !$order) {
    $order = commerce_cart_order_new($user->uid);
  }
  return $order;
}

function commerce_split_checkout_commerce_get_all_cart_orders($uid) {
  $orders = array();
  
  // Create an array of valid shopping cart order statuses.
  $status_ids = array_keys(commerce_order_statuses(array('cart' => TRUE)));
  
  // If a customer uid was specified...
  if ($uid) {
    // get a list of all of the shopping carts associated with this user
    $result = db_query('SELECT order_id FROM {commerce_order} WHERE uid = :uid AND status IN (:status_ids) ORDER BY order_id DESC', array(':uid' => $uid, ':status_ids' => $status_ids))->fetchAll();
  }
  else {
    // Otherwise look for a shopping cart order ID in the session.
    if (commerce_cart_order_session_exists()) {
      // We can't trust a user's session to contain only their order IDs, as
      // they could've edited their session to try and gain access to someone
      // else's order data. Therefore, this query restricts its search to
      // orders assigned to anonymous users using the same IP address to try
      // and mitigate risk as much as possible.
      $result = db_query('SELECT order_id FROM {commerce_order} WHERE order_id IN (:order_ids) AND uid = 0 AND hostname = :hostname AND status IN (:status_ids) ORDER BY order_id DESC', array(':order_ids' => commerce_cart_order_session_order_ids(), ':hostname' => ip_address(), ':status_ids' => $status_ids))->fetchAll();
    }
  }
  
  if (isset($result)) {
    foreach($result as $record) {
      $orders[] = commerce_order_load($record->order_id);
      //dpm($order);
    }
  }
  
  return $orders;
}

/**
 * restore a "saved for later" cart order back to normal cart status
 */
function commerce_split_checkout_restore_saved_cart($order, $message = false) {
  if ($order->status=='saved_cart') {
    if ($message) {
      drupal_set_message(t("The shopping cart items you had previously selected to \"Save for later\" have been restored to your cart."));
    }
    commerce_order_status_update($order, 'cart');
  }
}


function commerce_split_checkout_check_for_saved_carts($form = false) {
  global $user;
  $cart_order = @commerce_cart_order_load($user->uid);
  $all_cart_orders = commerce_split_checkout_commerce_get_all_cart_orders($user->uid);
  
  //assume there's no reason to not restore a cart order when this function is run if the current order is empty
  if ($cart_order) {
    $auto_restore = count($cart_order->commerce_line_items)>0?false:true;
  }
  
  // dpm($cart_order);
  // dpm($all_cart_orders);
  
  foreach($all_cart_orders as $order) {
  
    if ($order->order_number != $cart_order->order_number) {
        
      //determine size of order
      $quantity = 0;
      $wrapper = entity_metadata_wrapper('commerce_order', $order);
      foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
        $quantity += $line_item_wrapper->quantity->value();
      }
      
      if ($quantity > 0 && $auto_restore) {
        commerce_order_delete($cart_order->order_id);
        commerce_split_checkout_restore_saved_cart($order, true);
        
        //TODO: cleaner referesh of the current page?
        $path = $_SERVER['REQUEST_URI'];
        if ($path[0] == "/") {
          $pathparts = explode("/",$path);
          unset($pathparts[0]);
          $path = implode("/",$pathparts);
        }
        drupal_goto($path);
        
        return;
      }
      
    }
    
  }
  
}