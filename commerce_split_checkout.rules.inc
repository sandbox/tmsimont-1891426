<?php


/**
 * @file
 * Rules integration for Commerce Split Checkout.
 */

/**
 * Implements hook_rules_action_info().
 *
 * Provides an action to restore a user's "saved cart" order
 */
function commerce_split_checkout_rules_action_info() {
  $actions = array();
  $actions['commerce_split_checkout_restore_saved_carts'] = array(
    'label' => t('Restore a user\'s saved cart order.'),
    'group' => t('Commerce Split Checkout'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order in checkout'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_split_checkout_rules_restore_saved_carts',
    ),
  );
  return $actions;
}

/**
 * Action callback: completes checkout for the given order.
 */
function commerce_split_checkout_rules_restore_saved_carts($order) {
  //$order is the just-completed order, check for a cart order with the same UID
  $cart_order = @commerce_cart_order_load($order->uid);
  
  //if this user has another cart order, set it's status back to cart (it should be "saved for later")
  if (isset($cart_order) && $cart_order && !empty($cart_order) && isset($cart_order->order_number)) {
    commerce_split_checkout_restore_saved_cart($cart_order, true);
  }
}